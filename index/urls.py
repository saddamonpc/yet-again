"""myprofile URL Configuration"""

from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='index-home'),
    path('profile/', views.profile, name='index-profile'),
    path('projects/', views.projects, name='index-projects'),
    path('friends/', views.friends, name='index-friends'),
]