from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'index/index.html')

def profile(request):
    return render(request, 'index/profile.html')

def projects(request):
    return render(request, 'index/projects.html')

def friends(request):
    return render(request, 'index/friends.html')